package com.metahorizon.user.controller;

import com.metahorizon.user.entity.User;
import com.metahorizon.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Asus on 5/20/2018.
 */
@Controller
public class UserController {
    private User user = new User();
    @Autowired
    private UserService userService;

    @GetMapping(value = "/user")
    public String getUser(Model model) {
        model.addAttribute("user", user);
        return "user";
    }

}
