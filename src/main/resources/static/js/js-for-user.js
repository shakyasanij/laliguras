/**
 * Created by Asus on 6/7/2018.
 */
$(document).ready(function () {
    $('#usertab').DataTable({
        "processing": true,
        "ajax": {
            "url": "/users",
            "type": "GET",
            "dataSrc": ""
        },
        "columns": [{
            "data": "name"
        }, {
            "data": "nationality"
        }, {
            "data": "email"
        }, {
            "data": "phonenumber"
        }, {
            "data": "address"
        }, {
            "data": "batch"
        }, {
            "data": "role"
        }, {
            data: null,
            className: "center",
            defaultContent: '<a href=""><ion-icon name="open"></ion-icon></a> '
        }],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                title: function () {
                    return "laliguras";
                },
                orientation: 'Portrait',
                pageSize: 'A1',
                titleAttr: 'PDF',
                label: "PDF"
            },
            {
                extend: 'print',
                titleAttr: 'PRINT'
            }
        ]
    });

    $("#addUser").submit(function (event) {
        //stop submit the form, we will post it manually.
        event.preventDefault();
        $("#acceptBtn").prop("disabled", false);
        fire_ajax_submit();
    });

    function fire_ajax_submit() {
        $.ajax({
            type: "POST",
            url: "/user",
            data: $("#addUser").serialize(),
            cache: false,
            success: function (result) {
                $('#notification').addClass("bg-info text-white");
                $('#notification').html("User Added Successfully");
                $('#addUser').trigger("reset");
            },
            error: function (e) {
                $('#notification').addClass("bg-danger text-white");
                $('#notification').html("Error Something wrong. Either Try Again or call customer service");
            }
        });
    }

})




